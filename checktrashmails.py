#!/bin/env python3
import os
import sys

parentDir  = os.path.abspath(os.path.dirname(__file__))
includeDir = os.path.join(parentDir, "include")
sys.path.append(includeDir)

import requests as req
import pandas as pd

if len(sys.argv) != 2:
    print("short usage: {} inputFile".format(sys.argv[0]), file=sys.stderr)
    exit(0)

if "--help" in sys.argv or "-h" in sys.argv:
    print(f"usage: {sys.argv[0]} inputFile" + \
"""
This script takes a file with a list of emails as a parameter,
obfuscates the emails, and checks, whether the email is a trash mail or invalid mail
using block-temporary-email service. Then creates two files invalidmails.csv and trashmails.csv.
Default inputFile format is emails, one email per line, but you may specify a custom separator

parameters:

--help -h   : show this message
--sep "SEP" : define a custom separator (not implemented yet, default \\n)""")
    exit(0)

inputFile = sys.argv[-1]
if (not os.path.isfile(inputFile)):
    print("error: file not found", file=sys.stderr)
    exit(1)

def domain(s):
    return s[s.find("@"):]

with open(inputFile) as f:
    data = f.read()

emails = [ x for x in data.split("\n") if x != '' ]
table = [ dict(zip(["email", "short", "isValid"], [x, "x" + domain(x), 0])) for x in emails ]
df = pd.DataFrame(table)

i = 0
while i < len(df):
    line = df.iloc[i]
    if line["isValid"] != 0:
        i += 1
        continue
    print("checking: " + line["short"], end=": ")
    try:
        r = req.get("https://block-temporary-email.com/check/email/" + line["short"].strip())
        if "error" in r.json():
            # retry on error
            r = req.get("https://block-temporary-email.com/check/email/" + line["email"].strip())
            if "error" in r.json():
                # mark mail as invalid and print the error if there is still an error
                df.loc[i, "isValid"] = 3
                i += 1
                print("error: {}".format(r.json()["error"]))
                continue
        df.loc[df["short"] == line["short"], "isValid"] = 1 if r.json()["temporary"] == False else 2
        print("is_temporary={} emails_left=[{}/{}]".format(str(r.json()["temporary"]), len(df.loc[df["isValid"] == 0]), len(df)))
        i += 1
    except Exception as e:
        print("try again")
        print(r.json())
        print(e)

df.loc[df["isValid"] == 2].to_csv("trashmails.csv", sep=",", columns=["email"], index=False)
df.loc[df["isValid"] == 3].to_csv("invalidmails.csv", sep=",", columns=["email"], index=False)
